function makeAjaxRequest(url) {
    return new Promise(function (resolve, reject) {
      const xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);
            resolve(response);
          } else {
            reject(new Error("Failed to fetch data"));
          }
        }
      };
      xhr.open("GET", url, true);
      xhr.send();
    });
  }
  
  function fetchCharacterData(characterURL) {
    return makeAjaxRequest(characterURL);
  }
  
  function displayFilm(filmsListDiv, film) {
    const filmContainer = document.createElement("div");
    filmContainer.classList.add("filmContainer");
    filmContainer.innerHTML = "<h2>Эпизод " + film.episodeId + ": " + film.name + "</h2>" +
      "<p>" + film.openingCrawl + "</p>";
    filmsListDiv.appendChild(filmContainer);
  
    const charactersPromises = film.characters.map(fetchCharacterData);
  
    const loadingAnimation = document.createElement("div");
    loadingAnimation.classList.add("loading");
    filmContainer.appendChild(loadingAnimation);
  
    Promise.all(charactersPromises)
      .then(function (charactersData) {
        filmContainer.removeChild(loadingAnimation);
  
        const charactersNames = charactersData.map(character => character.name);
        const characterInfo = document.createElement("div");
        characterInfo.classList.add("characterInfo");
        characterInfo.innerHTML = "<p>Персонажи: " + charactersNames.join(', ') + "</p>";
        filmContainer.appendChild(characterInfo);
      })
      .catch(function (error) {
        console.error("Ошибка при получении данных о персонажах:", error);
      });
  }
  
  makeAjaxRequest("https://ajax.test-danit.com/api/swapi/films")
    .then(function (filmsData) {
      const filmsListDiv = document.getElementById("filmsList");
      filmsData.forEach(film => displayFilm(filmsListDiv, film));
    })
    .catch(function (error) {
      console.error("Error fetching films data:", error);
    });
  